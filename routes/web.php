<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {

    Route::get('/',  'DashboardController@show' )->name('admin.dashboard');

    Route::get('/login',  'LoginController@index' )->name('admin.login');
    Route::post('/login', 'LoginController@login')->name('admin.dologin');

    
    Route::get('/menus',  'MenuController@index' )->name('admin.menu');
    Route::post('/settings',  'SettingController@store' );
    Route::get('/setlang/{lang?}',  'SettingController@setlang' )->name('admin.setlang');


    Route::get('/users/search', 'UserController@search');
    Route::get('/users/datatable', 'UserController@datatable')->name('admin.users.datatable'); 
    Route::resource('/users', 'UserController', ['names' => [
        'index' => 'admin.users',
        'create' => 'admin.users.create',
        'update' => 'admin.users.update',
        
    ]]);
    
    
    Route::get('/rolls/search', 'RollController@search');
    Route::get('/rolls/datatable', 'RollController@datatable')->name('admin.rolls.datatable'); 
    Route::resource('/rolls', 'RollController', ['names' => [
        'index' => 'admin.rolls',
        'create' => 'admin.rolls.create',
        'update' => 'admin.rolls.update',
        
    ]]);



    Route::get('/permissions/search', 'PermissionsController@search');
    Route::get('/permissions/datatable', 'PermissionsController@datatable')->name('admin.permissions.datatable'); 
    Route::resource('/permissions', 'PermissionsController', ['names' => [
        'index' => 'admin.permissions',
        'create' => 'admin.permissions.create',
        'update' => 'admin.permissions.update',
        
    ]]);


    Route::get('/rollpermissions/{module?}',  'RollpermissionsController@index' )->name('admin.rollpermissions');
    Route::post('/rollpermissions/update',  'RollpermissionsController@updateRollpermissions' )->name('admin.rollpermissions.update');


});


 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
