<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Permissions extends Model

{
    protected $table = "permissions";
    protected $fillable = [
        'title' 
    ];

    public function rollpermissions()
    {
        return $this->hasMany('App\Rollpermissions', 'permission_id', 'id');
    }
}

