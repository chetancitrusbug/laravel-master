<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Rolls extends Model

{

    protected $table = "rolls";

    protected $fillable = [

        'title' 
    ];

   
    public function rollpermissions()
    {
        return $this->hasMany('App\Rollpermissions', 'roll_id', 'id');
    }
}

