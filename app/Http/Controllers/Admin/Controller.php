<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Session;
use DataTables;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    protected $user;
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
 
    public function __construct() {
        
       $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
       });
    } 

    public function index(){ 

        if(!canAccess($this->context,'View') ){
            return redirect()->route('admin.login');
        }
       
        return view('admin.'.$this->context.'.index');
    }

    public function create(Request $request)
    {
        
		return view('admin.'.$this->context.'.create');
    }
    
    public function show($id,Request $request)
    {
		$item = $this->modal::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->route('admin.'.$this->context.'s');
		}
		return view('admin.'.$this->context.'.show',compact('item'));
    }

    public function edit($id,Request $request)
    {
        $result = array();
        $item = $this->modal::findOrFail($id);
        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;	
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->route('admin.'.$this->context.'s');
        }
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            return view('admin.'.$this->context.'.edit', compact('item'));
        }
    }

    public function datatable(Request $request) {
        
        $record = $this->modal::where("id",">",0);
		if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '') {
            $record->where('status',$request->get('status'));
        }
		if ($request->has('id') && $request->get('id') != '' ) {
            $record->where('id',$request->get('id'));
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
        return Datatables::of($record)->make(true);
    }

    public function store(Request $request)
    {
        $result = array();		
		$varr = [
            'title' => 'required',
        ];

        $input = $request->except(['']);
        $item = $this->modal::create($input);
        
        if($item){
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			if($request->has('previous_url') && $request->previous_url != ""){
				return redirect($request->previous_url);
			}
            return redirect()->route('admin.'.$this->context.'s',['id',$item->id]);
        }

    }

    public function update($id, Request $request)
    {
         
        $result = array();
		//$this->validate($request,$varr,[],trans('issue.label'));
        $item = $this->modal::where("id",$id)->first();
        $requestData = $request->except(['']);
		if($item){
            $item->update($requestData);
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			if($request->has('previous_url') && $request->previous_url != ""){
				return redirect($request->previous_url);
			}
            return redirect()->route('admin.'.$this->context.'s',['id',$item->id]);
        }   
    }

    public function destroy($id,Request $request)
    {
        $item = $this->modal::where("id",$id)->first();
		$is_hard_delete = 0;
		if(!$item){
			$item = $this->modal::withTrashed()->where('id',$id)->first();
			$is_hard_delete = 1;
		}
		$roll_id = 0;
        $result = array();
        if($item){
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;
			$roll_id = $item->id;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			if($roll_id){
                return redirect()->route('admin.'.$this->context.'s',['id',$item->id]);
			}else{
                return redirect()->route('admin.'.$this->context.'s');
			}
		}	
    }

}
