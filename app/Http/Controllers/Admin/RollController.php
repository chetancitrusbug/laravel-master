<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
    
use Illuminate\Support\Str;

use Illuminate\Support\Facades\View;

class RollController extends Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->context = 'roll';
        $this->modal = 'App\\'.ucfirst(Str::plural($this->context));
        
        View::share('context',  $this->context);
       
    } 

    
}
