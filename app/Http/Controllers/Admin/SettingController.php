<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App;
use App\Settings;
use Illuminate\Http\Request;


 
class SettingController extends Controller
{
    
     

    public static function index(){ 
        return view('admin.dashboard.index');
    }


    public function store(Request $request){
        
        $setting = Settings::where('fkey',$request->key)->first();
        if($setting){
            $setting->fvalue = $request->value;
            $setting->save();
        }else{
            Settings::create([
                'fkey'=> $request->key,
                'fvalue' => $request->value,
            ]);
        }
        setSettings($request->key, $request->value);
        return($setting);
    }

    public function setlang($lang){
        $setting = Settings::where('fkey','active_lang')->first();
        if($setting){
            $setting->fvalue = $lang;
            $setting->save();
        }else{
            Settings::create([
                'fkey'=> 'active_lang',
                'fvalue' => $lang,
            ]);
        }
        setSettings('active_lang', $lang);
        

      
        
        return back();
    }

}
