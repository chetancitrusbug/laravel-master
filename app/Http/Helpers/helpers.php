<?php

use App\Languages;
use App\Settings;
use App\Rollpermissions;
use Illuminate\Support\Facades\Auth;

function canAccess($module,$action){
    $user = Auth::user();
    $rolls = $user->rolls;
    $pass = false;
    if(count($rolls)>0){
        foreach($rolls as $roll){
            $rollpermissions = $roll->rollpermissions()->where('module',$module)->get();   
            if(count($rollpermissions)>0){
                foreach($rollpermissions as $rollpermission){
                    $title = $rollpermission->permission->title;
                    if( strtolower($title)  == strtolower($action) && $rollpermission->value == 1 ){
                        $pass = true;
                    } 
                }
            }else{
                $rollpermissions = $roll->rollpermissions()->where('module','global')->get();   
                if(count($rollpermissions)>0){
                    foreach($rollpermissions as $rollpermission){
                        $title = $rollpermission->permission->title;
                        if( strtolower($title)  == strtolower($action) && $rollpermission->value == 1 ){
                            $pass = true;
                        } 
                    }
                } 
            }
            
        }
    }
    return $pass;
}


function renderAdminMenu($items, $sub = false){
    if(count($items) > 0){
        if($sub){
            $output = '<ul class="menu-content">';
        }else{
            $output = '<ul id="main-menu-navigation" data-menu="menu-navigation" data-scroll-to-active="true" class="navigation navigation-main">';
        }
        
        foreach($items as $item){
        //  dd($item->children);

            if($sub){
                $output .= ' <li class="'. ( ( isset($item->children) && count($item->children) > 0) ? 'has-sub': '' ) . '">';
            }else{
                $output .= ' <li class=" nav-item '. ( ( isset($item->children) && count($item->children) > 0) ? 'has-sub': '' ) . ' ">';
            }
                
            if($sub){

                if ( isset($item->children) && count($item->children) > 0){
                    $output .='<a href="#" >'; 
                }else{
                    $output .='<a href="'.$item->href.'" target="'.$item->target.'" title="'.$item->title.'" class="menu-item" >';
                }
               
            }else{
                

                if ( isset($item->children) && count($item->children) > 0){
                    $output .='<a href="#" >';
                }else{
                    $output .='<a href="'.$item->href.'" target="'.$item->target.'" title="'.$item->title.'" >';
                }

            }

                    $output .='<i class="'.$item->icon.'"></i><span data-i18n="" class="menu-title">'.$item->text.'</span>';
                                    
                $output .='</a>';
                             
                if(isset($item->children) && count($item->children) > 0){
                    $output .= renderAdminMenu($item->children , true);
                } 
                            
            $output .='</li>';
        }
        $output .= '</ul>';
    }

    return $output;
}

function Text($text, $values = null){
    $text = strtoupper(Str::slug($text,'_'));
   // dd($text);
}

function getLangs(){
    return Languages::get();
}

function setSettings($fkey, $value){
    $context = env('APP_KEY');
    
    session([$context.'.'.$fkey => $value]);
}

function getSettings($fkey,  $default = null){

    $context = env('APP_KEY');
        if(( session($context.'.'.$fkey)) !== false && ( session($context.'.'.$fkey)) !== null ){
            $return =  session($context.'.'.$fkey);
        }else{
            $setting = Settings::where('fkey',$fkey)->first();
            if($setting){
                session([$context.'.'.$fkey => $setting->fvalue]);
                $return =  $setting->fvalue;  
            }else{
                session([$context.'.'.$fkey => $default]);
                $return =  $default;  
            }
        }
    return $return ;

}

function checkpermission($roll, $permission, $module){
    $context = env('APP_KEY');
    //$fkey = $module."_roll_".$roll."_permission_".$permission;
    $fkey = $module.".roll_".$roll.".permission_".$permission;

    if(( session($context.'.'.$fkey)) !== false && ( session($context.'.'.$fkey)) !== null ){
        $return =  session($context.'.'.$fkey);
    }else{
        $rollpermission = Rollpermissions::where('roll_id',$roll)->where('permission_id',$permission)->where('module',$module)->first();
        if($rollpermission){
            if($rollpermission->value == 1){
                session([$context.'.'.$fkey => true]);
                $return =  true;  
            }else{
                //session([$context.'.'.$fkey => false]);
                $return =  false;  
            }
        }else{
            if($module != 'global'){
                $rollpermission = Rollpermissions::where('roll_id',$roll)->where('permission_id',$permission)->where('module','global')->first();
                if($rollpermission){
                    if($rollpermission->value == 1){
                        session([$context.'.'.$fkey => true]);
                        $return =  true;  
                    }else{
                        //session([$context.'.'.$fkey => false]);
                        $return =  false;  
                    }
                }else{
                    //session([$context.'.'.$fkey => false]);
                    $return =  false;  
                }
            }else{
                //session([$context.'.'.$fkey => false]);
                $return =  false;  
            }
        }
    }
    return $return ;
}

?>