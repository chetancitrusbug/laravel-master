<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Userrolls extends Model

{
    protected $table = "user_rolls";
    protected $fillable = [
        'user_id',
        'roll_id' 
    ];  

    public function rollpermissions()
    {
        return $this->hasMany('App\Rollpermissions', 'roll_id', 'roll_id');
    }

}

