<?php
return [
    'edit_item' => 'Edit Item',
    'text' => 'Text',
    'url' => 'URL',
    'target' => 'Target',
    'tooltip' => 'Tooltips',
    'selectrolls' => 'Select Rolls',
    'update' => 'Update',
    'save' => 'Save',
    'add' => 'Add',
    'add_new' => 'Add New',
    'id' => "ID",
    'title' => "Title",
    'created' => "Created at",
    'action' => "Action",
    'delete' => "Delete",
    'create' => "Create",
    'clear_form' => "Clear Form",
    'datatable' => [
        'show' => "Show",
        'entries' => "Entries",
        'search' => "Search",
        'paginate' => [
            'first' => "First",
            'previous' => "Previous",
            'next' => "Next",
            'last' => "Last",
        ],
        'showing' => 'Showing',
        'to' => 'To',
        'of' => 'Of',
        'small_entries' => 'entries'
    ]
];

?>