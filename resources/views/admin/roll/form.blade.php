<?php
 $hid =	"";
 if(isset($item)){
	 $hid =	"_".$item->id;
 }
 ?>
 

 
<div class="row ">

    {!! Form::hidden('previous_url', str_replace(url('/'), '', url()->previous()) , ['id'=>'previous_url']) !!}
    <div class="col-md-6">
      @if(isset($survey))
        {!! Form::hidden('survey_id',$survey->id, ['class' => 'form-control','id'=>'survey_id']) !!}
      @endif 
		{!! Form::hidden('status',"new", ['class' => 'form-control']) !!}
		
		
	  
		
		<div class="form-group {{ $errors->has('location') ? ' has-error' : ''}}">
            <label for="location" >
                <span class="field_compulsory">*</span>@lang($context.'.create.field.title')
            </label>
            <div >
			{!! Form::text('title', null, ['class' => 'title','id'=>'title','style'=>'width:100%']) !!}
			
				{!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
       
 
		
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.create'), ['class' => 'btn btn-primary form_submit']) !!}
        {{ Form::reset(trans('common.clear_form'), ['class' => 'btn btn-light']) }}
 
        </div>
   
       
    </div>
   
    
</div>



@push('js')
<script>
 
 

</script>


@endpush


